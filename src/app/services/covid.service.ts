import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CovidService {
  constructor(private http:HttpClient) {
  }

  getQuery(query:string){
    const url:string=`https://coronavirus-19-api.herokuapp.com/${query}`;
      return this.http.get(url);
  }

  getPaises(){
    return this.getQuery('countries');
  }

  getOnePais(termino:string){
        return this.getQuery(`countries/${termino}`);
  }
}
