import { Component, OnInit } from '@angular/core';
import { CovidService } from '../services/covid.service';
import { Observable, OperatorFunction } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
@Component({
  selector: 'app-form',
  templateUrl: './form.component.html'
})
export class FormComponent implements OnInit {
  paises: any[] = [];
  public model: any;
  info: string = "";
  data: any;
  clickedItem: any;
  isLoading?: Boolean;
  constructor(private covidservice: CovidService) {

  }
  // buscar en el arreglo ng bootstrap
  search: OperatorFunction<string, readonly string[]> = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      map(term => term.length < 2 ? []
        : this.paises.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
    );
  //al seleccionar un item ng bootstrap
  selectedItem($event: any) {
    this.clickedItem = $event.item;
    this.isLoading = true;
    //buscar el pais al seleccionar metodo getOnePais
    this.covidservice.getOnePais(this.clickedItem)
      .subscribe((response: any) => {
        this.data = response;
        this.outputData();
      }).add(() => this.isLoading = false);

  }
  outputData() {
    let paisnombre = this.data.country;
    let arreglo: any = {
      "Fallecidos:": this.data.deaths,
      "Facellidos este dia:": this.data.todayDeaths,
      "Casos:": this.data.cases,
      "Casos Activos:": this.data.active,
      "Nuevos casos:": this.data.todayCases,
      "Recuperados:": this.data.recovered,
      "Estados Criticos:": this.data.critical
    };
    let html = "";
    if (typeof (this.data) === 'object') {// si el resultado es objeto
      for (var indice in arreglo) {
        if (arreglo[indice] != null) {
          html += "<strong>" + indice + "</strong><span id='texto'>" + arreglo[indice] + "</span><br/>";
        }
      }
    }
    else { // si no lo es haz esto
      html += "País no encontrado";
    }
    this.info = `<div class="card border-light mb-3 mx-auto anchocard">
    <div class="card-header"><h2>${!paisnombre ? "" : paisnombre}</h2></div>
    <div class="card-body mx-auto">
      ${html}
    </div>
      </div>
    `;

  }
  //funcion click button
  buscarPais(termino: string) {
    //validamos que se haya ingresado algo en el input 
    if (termino == null || termino.length == 0 || /^\s+$/.test(termino)) {
      alert('ingrese un pais');
    }
    else {
      this.isLoading = true;
      this.covidservice.getOnePais(termino)
        .subscribe((response: any) => {
          this.data = response;
          this.outputData();
        },
          (error) => {
            this.data = "Country not found";
            this.outputData();
          },
        ).add(() => this.isLoading = false);
    }
  }
  ngOnInit() {

    //llamando la funcion getPaises del service
    this.covidservice.getPaises()
      .subscribe((data: any) => {
        for (let i in data) {
          this.paises.push(data[i]["country"]);
        }
      });



  }
}


